from django import forms
from django.contrib import admin
from .models import *

class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'password')

class CommentsAdmin(admin.ModelAdmin):
    list_display = ('i', 'pr', 'com')

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)

class ProductAdmin(admin.ModelAdmin):
    list_display = ('category', 'product', 'price', 'massa', 'year', 'slug')
admin.site.register(User, UserAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Comment, CommentsAdmin)
